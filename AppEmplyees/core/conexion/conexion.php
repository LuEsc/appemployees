
<?php

/**
 * Declaramos aquellos metodos en privado los cuales no queremos ejecutar fuera de esta clase
 * y como publicos aquellos que en cualquier momento serán instaciados fuera de la clase.
 */

    class Conexion extends PDO{
       private $server = 'localhost';
       private $user = 'root';
       private $password = '';
       private $database = 'api_restphp';
       private $port ;


       public function __construct(){
           try{
               parent::__construct('mysql:host=' . $this->server . ';dbname=' . $this->database . ';charset=utf8', $this->user, $this->password,
                array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));

           }catch(PDOexception $e){
               echo "error: " . $e->getMessage();
               exit;
           }
       }

       

}


?>