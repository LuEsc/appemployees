<?php

include 'AppEmplyees/core/conexion/conexion.php';

$pdo = new Conexion();

/**
 * Funcion GET para obtener todos los datos de nuestra tabla employee en la base de datos
 */
if($_SERVER['REQUEST_METHOD'] == 'GET'){

    if(isset($_GET['emp_no'])){

        $sql = $pdo->prepare("SELECT * FROM employees WHERE emp_no=:emp_no");
        $sql->bindValue(':emp_no', $_GET['emp_no']);
        $sql->execute();
        $sql->setFetchMode(PDO::FETCH_ASSOC);
        header("HTTP/1.1 200 OK");
        echo json_encode($sql->fetchAll());
        exit();

    }else {

        $sql = $pdo->prepare("SELECT * FROM employees");
        $sql->execute();
        $sql->setFetchMode(PDO::FETCH_ASSOC);
        header("HTTP/1.1 200 OK");
        echo json_encode($sql->fetchAll());
        exit();

    }   
}

if($_SERVER['REQUEST_METHOD'] == 'POST'){

    $sql = "INSERT INTO employees (birth_day, first_name, last_name) VALUES (:birth_day, :first_name, :last_name)";
    
}

?>